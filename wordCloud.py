#coding=utf-8
# 开发作者: HeBugui
# 开发时间 :2022/4/1 16:38
import jieba #分词
from matplotlib import pyplot as plt #绘图，数据可视化
from wordcloud import WordCloud #词云
from PIL import Image #图片处理
import numpy as np #矩阵运算


def word_cloud(text,path,title):

    #中文编码
    plt.rcParams['font.sans-serif'] = [u'simHei']
    plt.rcParams['axes.unicode_minus'] = False

    # 分词
    cut = jieba.cut(text)
    string = ' '.join(cut)

    img = Image.open(r'.\static\assets\img\beauty.jpg') #打开遮罩图片
    img_array = np.array(img) #将图片转换为数组
    wc = WordCloud(
        background_color='white',
        mask=img_array,
        font_path="simhei.ttf" #字体所在位置 C:\Windows\Fonts
    )
    wc.generate_from_text(string)

    #绘制图片
    fig = plt.figure(1)
    plt.imshow(wc)
    plt.axis('off') #是否显示坐标轴
    plt.title('学号：'+ title,y=-0.2)
    path = path + '学号：'+ title +'.jpg'
    print(path)
    # plt.show()
    plt.savefig(path,dpi=600)