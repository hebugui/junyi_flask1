import sqlite3

from flask import Flask, render_template, request, redirect, url_for

import exercise_info
import problemlog
import numpy as np
import pandas as pd

app = Flask(__name__)


@app.route('/')
def index():
    return render_template('index.html')

@app.route('/index')
def home():
    return index()

@app.route('/team')
def team():  # put application's code here
    return render_template('team.html')

@app.route('/exercise')
def exercise():
    area_count = exercise_info.area_count()
    topic_count = exercise_info.topic_count()
    live_count = exercise_info.live_count()
    prerequisites_count = exercise_info.prerequisites_count()
    exercise_topic_count = exercise_info.exercise_topic_count().tolist()
    exercise_topic_index = list(exercise_info.exercise_topic_count().index)
    exercise_area_count = exercise_info.exercise_area_count().tolist()
    exercise_area_index = list(exercise_info.exercise_area_count().index)
    exercise_prerequisites_count = exercise_info.exercise_prerequisites_count().tolist()
    exercise_prerequisites_index = list(exercise_info.exercise_prerequisites_count().index)
    # Exercises distribution on area in knowledge map
    series_data = exercise_info.exercise_distribution_by_area()
    series_data_topic = exercise_info.exercise_distribution_by_topic()
    return render_template('exercise.html',area_count = area_count,topic_count = topic_count,live_count = live_count,prerequisites_count = prerequisites_count,exercise_topic_count=exercise_topic_count,exercise_topic_index=exercise_topic_index,exercise_area_count=exercise_area_count,
                           exercise_area_index=exercise_area_index,exercise_prerequisites_count=exercise_prerequisites_count[-40:],exercise_prerequisites_index=exercise_prerequisites_index[-40:],series_data=series_data,series_data_topic=series_data_topic)

@app.route('/problemlogs')
def problemlogs():
    excellent_stu = problemlog.excellent_student()
    nums = list(excellent_stu)
    # print(type(nums),nums)
    ids = list(excellent_stu.index)
    problemlog_exercise = problemlog.init_dataset()
    num_points  = problemlog.cluster(problemlog_exercise)

    num_max = num_points['做题数量'].max()+10
    points_max = num_points['获得积分数'].max()+10
    num_points = np.array(num_points)[:, 1:].tolist()
    # print(num_points)
    # 最喜欢的题目类型（话题领域和名称）
    list_topic,list_area,list_exercise_name = problemlog.alllike_topic_area(problemlog_exercise)
    #Percent of mastered exercises
    master_percentage = []
    mastered,total = problemlog.count_master_percentage()
    dict1 = {'value': mastered, 'name': 'mastered'}
    dict2 = {'value': total-mastered, 'name': 'not mastered'}
    master_percentage.append(dict1)
    master_percentage.append(dict2)
    #散点图（正确率，做题数量）
    num_rate = pd.read_csv('junyi_data/problemlog/student_correct.csv')
    num_rate = np.array(num_rate[['num','correct_rate']]).tolist()
    # print(num_rate)
    return render_template('problemlog.html',ids=ids,nums=nums,num_points=num_points,num_max=num_max,points_max=points_max,list_topic=list_topic,list_area=list_area,list_exercise_name=list_exercise_name,master_percentage=master_percentage,num_rate=num_rate)

@app.route('/recommend')
def recommend():  #默认给做题数量最多的学生推荐题目和刻画词云

    problemlog_exercise = problemlog.init_dataset()
    # problemlog.most_student(problemlog_exercise)#更新词云
    max_similarity, rec_student_id, rec_exercises_info,student_id = problemlog.recommendation_student(problemlog_exercise)#推荐练习
    len1 = len(rec_exercises_info)
    return render_template('recommend.html',similarty=max_similarity,stu_id=rec_student_id,exercises=rec_exercises_info,your_id=student_id,len=len1)

@app.route('/searchById',methods=['post'])
def searchById():
    user_info = request.values.to_dict()
    uid = user_info.get("uid")
    print('*'*100,uid,type(uid))
    uid = int(uid)
    #更新词云图
    problemlog_exercise = problemlog.init_dataset()
    # problemlog.most_student(problemlog_exercise,student_id=uid)
    #更新练习推荐
    max_similarity, rec_student_id, rec_exercises_info, student_id = problemlog.recommendation_student(problemlog_exercise,student_id=uid)
    len1 = len(rec_exercises_info)
    return render_template('recommend.html', similarty=max_similarity, stu_id=rec_student_id,
                           exercises=rec_exercises_info, your_id=student_id, len=len1)
    # return redirect(url_for('recommend',uid=uid))


if __name__ == '__main__':
    app.run(debug=False,threaded=True)
